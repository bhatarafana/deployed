// import React, { useState } from 'react';
// import { Link } from 'react-router-dom';
// import '@fontsource/poppins';
// import "./css/file.css"
import { Button } from "react-bootstrap";
// import {
//   Container,
//   Form,
//   // Modal
// } from 'react-bootstrap';
import AvatarImageCropper from "react-avatar-image-cropper";
import axios from "axios";
import { useSelector } from "react-redux";
import { useEffect, useState } from "react";
// export default function profile() {
//   const setImage = (file) => {
//     console.log(file)
//   }

//   const actions = [
//     <Button variant="info" key={0}>Apply</Button>,
//     <Button variant="dark" key={1}>Cancel</Button>,
//   ]
// }

export default function Profile() {
  const { user } = useSelector((state) => state.user);
  const [name, setName] = useState("");
  const [city, setCity] = useState("");
  const [address, setAddress] = useState("");
  const [phone, setPhone] = useState("");

  const cities = [
    "Probolinggo",
    "Lumajang",
    "Pasuruan",
    "Jember",
    "Malang",
    "Kediri",
    "Yogyakarta",
    "Bandung",
    "Bogor",
  ];

  useEffect(() => {
    const getUserProfile = async () => {
      try {
        const response = await axios.get(`/api/profile/${user.id}`);
        const { data } = response.data;

        if (data.full_name) {
          setName(data.full_name);
        }

        if (data.city) {
          setCity(data.city);
        }

        if (data.address) {
          setAddress(data.address);
        }

        if (data.phone) {
          setPhone(data.phone);
        }
      } catch (error) {
        console.error(error);
      }
    };
    getUserProfile();
  }, [user.id]);

  const setImage = (file) => {
    console.log(file);
  };

  const actions = [
    <Button variant="info" key={0}>
      Apply
    </Button>,
    <Button variant="dark" key={1}>
      Cancel
    </Button>,
  ];

  const updateProfile = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.put(`/api/profile/${user.id}`);
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <div className="container">
        <br />
        <AvatarImageCropper apply={setImage} actions={actions} />
        <div className="row mt-5">
          <div className="col-md-3">
            <a href="/">
              <i className="bi bi-arrow-left offset-md-5"></i>
            </a>
          </div>

          <form onSubmit={updateProfile} action="#" className="col-md-6">
            <div className="col-md mb-3">
              <label htmlFor="nm_produk" className="form-label">
                Nama*
              </label>

              <input
                type="type"
                className="form-control"
                id="nm_produk"
                placeholder="Nama"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </div>

            <div className="col-md mb-3">
              <label htmlFor="kategori" className="form-label">
                Kota*
              </label>

              <select
                className="form-control"
                id="kota"
                value={city}
                onChange={(e) => setCity(e.target.value)}
              >
                <option value={""}>Pilih kota</option>

                {cities.sort().map((cityName) => (
                  <option key={"kota/kabupaten " + cityName} value={cityName}>
                    {cityName}
                  </option>
                ))}
              </select>
            </div>

            <div className="col-md mb-3">
              <label htmlFor="deskripsi" className="form-label">
                Alamat*
              </label>

              <textarea
                className="form-control"
                id="deskripsi"
                rows="3"
                placeholder="Contoh: Jalan Ikan Hiu 33"
                value={address}
                onChange={(e) => setAddress(e.target.value)}
              ></textarea>
            </div>

            <div className="col-md mb-3">
              <label htmlFor="nm_produk" className="form-label">
                No.Handphone*{" "}
              </label>

              <input
                type="type"
                className="form-control"
                id="nm_produk"
                placeholder="+62854263762"
                value={phone}
                onChange={(e) => setPhone(e.target.value)}
              />
            </div>

            {/* <div className="col-md mb-3">

            <label htmlFor="foto" className="form-label">Foto Produk</label><br />

            <label><img src='/assets/img/Group_1.png' alt='' /><input type={'file'} hidden /></label>

        </div> */}

            <div className="row">
              <div className="col-md mb-3 d-grid">
                <button type="submit" className="btn btn-primary">
                  submit
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

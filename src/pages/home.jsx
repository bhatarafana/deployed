import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { clearSelected, getProducts } from "../app/features/productsSlice";
import gift from "../assets/img/gift.png";
import undrawSelection from "../assets/img/undraw_selection_re_ycpo 1.png";
import "../assets/style.css";
import "../assets/img/ramadanbanner.png";
import { baseURL } from "..";
import { Link, useNavigate } from "react-router-dom";
import { getCategories } from "../app/features/categoriesSlice";

export default function Home() {
  const [category, setCategory] = useState("");
  const categories = useSelector((state) => state.categories);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);

  dispatch(clearSelected());

  useEffect(() => {
    dispatch(getCategories());
  }, [dispatch]);
  useEffect(() => {
    dispatch(getProducts({ category }));
    // const getCategories = async () => {
    //   try {
    //     const response = await axios.get("/api/categories");
    //     const { data } = response.data;
    //     setCategories(data);
    //   } catch (error) {
    //     console.log(error);
    //   }
    // };
    // getCategories();
  }, [category, dispatch]);

  const products = useSelector((state) => state.products);

  const navigateProduct = (id) => {
    navigate(`/productpage/${id}`);
  };

  return (
    <>
      <div className="pb-5">
        <div className="container section-5 text-center">
          <h2 className="h2-section-5 justify-content-start" id="banner">
            Bulan Ramadan Banyak Diskon!
          </h2>
          <h3 className="p-section-5 mt-2">Diskon Hingga 60%</h3>
          <img className="gift" src={gift} alt="gift box" />
        </div>

        <div className="container pb-5">
          <div className="category">
            <h3>Telusuri Kategori</h3>
            <div className="category__list">
              <button
                type="button"
                className="btn btn-color-theme pl-3 pr-3"
                onClick={(e) => setCategory("")}
              >
                <FontAwesomeIcon icon="fa-search" className="mr-2" /> Semua
              </button>
              {categories.status === "success" &&
                categories.categories.data.map((arr) => (
                  <button
                    key={arr.id}
                    type="button"
                    className="btn btn-color-theme pl-3 pr-3"
                    onClick={(e) => setCategory(arr.id)}
                  >
                    <FontAwesomeIcon icon="fa-search" className="mr-2" />{" "}
                    {arr.name}
                  </button>
                ))}
              {/* <button type="button" className="btn btn-color-theme pl-3 pr-3 ">
                <FontAwesomeIcon icon="fa-search" className="mr-2" /> Hobi
              </button>
              <button type="button" className="btn btn-color-theme pl-3 pr-3">
                <FontAwesomeIcon icon="fa-search" className="mr-2" /> Kendaraan
              </button>
              <button type="button" className="btn btn-color-theme pl-3 pr-3">
                <FontAwesomeIcon icon="fa-search" className="mr-2" /> Baju
              </button>
              <button type="button" className="btn btn-color-theme pl-3 pr-3">
                <FontAwesomeIcon icon="fa-search" className="mr-2" /> Elektronik
              </button>
              <button type="button" className="btn btn-color-theme pl-3 pr-3">
                <FontAwesomeIcon icon="fa-search" className="mr-2" /> Kesehatan
              </button> */}
            </div>
          </div>
          <div
            className={
              products.products.status && products.products.data?.length > 0
                ? "products"
                : "no-products"
            }
          >
            {products.products.status && products.products.data.length > 0 ? (
              <>
                {products.products.data.map((product) => (
                  <div
                    className={`card ${
                      user.user.id === product.merchant.id ? "product-self" : ""
                    }`}
                    key={product.id}
                    onClick={() => navigateProduct(product.id)}
                  >
                    {product.image.length > 0 ? (
                      <img
                        src={baseURL + product.image[0]?.path}
                        className="card-img-top"
                        alt={"Product " + product.name}
                        style={{ height: "100px", objectFit: "contain" }}
                      />
                    ) : (
                      <div
                        className="card-img-top"
                        style={{
                          height: "100px",
                          objectFit: "contain",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <FontAwesomeIcon icon="fa-image" size="5x" />
                      </div>
                    )}
                    <div className="card-body">
                      <p className="card-title">{product.name}</p>
                      <p className="card-text">{product.category.name}</p>
                      <p className="card-price">
                        {new Intl.NumberFormat("id-ID", {
                          style: "currency",
                          currency: "idr",
                        }).format(product.price)}
                      </p>
                    </div>
                  </div>
                ))}
              </>
            ) : (
              <div style={{ maxWidth: "290px" }}>
                <img src={undrawSelection} alt="Tidak ada produk" />
                <p className="text-center mt-2">
                  Produk masih tidak ada, jadilah yang pertama menjual produk di{" "}
                  <strong>SecondHand</strong>{" "}
                </p>
              </div>
            )}
          </div>
        </div>
      </div>

      <Link
        to={"/productinfo"}
        state="/"
        type="button"
        className="btn btn-color-theme pl-3 pr-3 button-jual"
      >
        <FontAwesomeIcon icon="fa-plus" className="mr-2" /> Jual
      </Link>
    </>
  );
}

import axios from "axios";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { login } from "../app/features/userSlice";
import cashier from "../assets/img/Rectangle-131.png";
import Cookie from "js-cookie";

import "../assets/signup_login.css";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorLogin, setErrorLogin] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const dispatch = useDispatch();

  const navigate = useNavigate();

  const loginForm = async (event) => {
    event.preventDefault();
    try {
      const response = await axios.post("/api/login", {
        email,
        password,
      });

      const data = response.data;
      setErrorLogin(false);
      dispatch(login({ token: data.token, data: data.data }));
      Cookie.set("token", data.token, { expires: 1 });
      axios.defaults.headers.common["Authorization"] = "Bearer " + data.token;
      navigate("/");
    } catch (error) {
      console.log(error);
      if (error.code === "ERR_BAD_REQUEST") {
        setErrorLogin(true);
        setErrorMessage(error.response.data.message);
      }
    }
  };

  return (
    <div className="App">
      <div className="row">
        <div className="col-sm-6 d-none d-sm-block position-relative">
          <img
            src={cashier}
            alt=""
            width={"100%"}
            height="100%"
            style={{ objectFit: "cover" }}
          />
          <h2 className="brand">
            Second
            <br />
            Hand.
          </h2>
        </div>
        <div className="col-sm-6 form-register">
          <form onSubmit={loginForm}>
            <h2>
              <strong>Masuk</strong>
            </h2>
            {errorLogin && (
              <div
                className="alert alert-danger alert-dismissible fade show"
                role="alert"
              >
                <p>{errorMessage}</p>
                <button
                  type="button"
                  className="close"
                  data-dismiss="alert"
                  aria-label="Close"
                  onClick={() => {
                    setErrorLogin(false);
                  }}
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            )}
            <div className="form-group ">
              <label htmlFor="exampleInputEmail1 ">Email</label>
              <input
                type="email"
                className="form-control"
                id="exampleInputEmail1"
                placeholder="Contoh: johndee@gmail.com"
                onChange={(e) => setEmail(e.target.value)}
                value={email}
                required
              />
            </div>
            <div className="form-group ">
              <label htmlFor="exampleInputPassword1 ">Password</label>
              <input
                type="password "
                className="form-control "
                id="exampleInputPassword1 "
                placeholder="Masukkan password"
                onChange={(e) => setPassword(e.target.value)}
                value={password}
                required
              />
            </div>
            <button type="submit " className="btn btn-primary w-100">
              Masuk
            </button>
            <p className="mt-4 text-center cta">
              <span>Belum punya akun?</span>{" "}
              <Link to={"/signup"}>Daftar di sini</Link>
            </p>
          </form>
        </div>
      </div>
    </div>
    // <div className="container col-lg-12">
    //   <div className="row">
    //     <img src={cashier} alt="" />
    //     <div
    //       className="col-lg-3"
    //       style={{ marginTop: "20%", marginLeft: "8%" }}
    //     >
    //       <h2>
    //         <strong>Masuk</strong>
    //       </h2>
    //       <form
    //         onSubmit={(event) =>
    //           this.login(event, this.state.email, this.state.password)
    //         }
    //       >
    //         <div className="form-group ">
    //           <label htmlFor="exampleInputEmail1 ">Email address</label>
    //           <input
    //             type="email "
    //             className="form-control "
    //             id="exampleInputEmail1 "
    //             aria-describedby="emailHelp "
    //             placeholder="Enter email "
    //             onChange={(e) => this.setState({ email: e.target.value })}
    //           />
    //           <small id="emailHelp " className="form-text text-muted ">
    //             We'll never share your email with anyone else.
    //           </small>
    //         </div>
    //         <div className="form-group ">
    //           <label htmlFor="exampleInputPassword1 ">Password</label>
    //           <input
    //             type="password "
    //             className="form-control "
    //             id="exampleInputPassword1 "
    //             placeholder="Password "
    //             onChange={(e) => this.setState({ password: e.target.value })}
    //           />
    //         </div>
    //         <button type="submit " className="btn btn-primary ">
    //           Submit
    //         </button>
    //       </form>
    //     </div>
    //   </div>
    // </div>
  );
};
export default Login;

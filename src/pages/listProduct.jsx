import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { Container, Row, Col, Card, ListGroup } from "react-bootstrap";
import { FaCube, FaRegHeart, FaDollarSign, FaPlus } from "react-icons/fa";

import "bootstrap/dist/css/bootstrap.min.css";
import "../assets/listProduct.css";
import { useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import undrawSelection from "../assets/img/undraw_selection_re_ycpo 1.png";
import { useState } from "react";
import axios from "axios";
import { useEffect } from "react";
import { baseURL } from "..";

function ListProduct() {
  const { user } = useSelector((state) => state.user);
  const [products, setProducts] = useState([]);
  const [interested, setInterested] = useState([]);
  const [sold, setSold] = useState([]);
  const [menuActive, setMenuActive] = useState("all");
  const menus = [
    {
      value: "all",
      icon: <FaCube className="mr-2" />,
      text: "Semua Product",
    },
    {
      value: "whislist",
      icon: <FaRegHeart className="mr-2" />,
      text: "Diminati",
    },
    {
      value: "sold",
      icon: <FaDollarSign className="mr-2" />,
      text: "Terjual",
    },
  ];

  const navigate = useNavigate();

  useEffect(() => {
    const getProductUser = async () => {
      try {
        const responseProduct = await axios.get("api/sellerproduct");

        const { data: products } = responseProduct.data;

        setProducts(products);

        const responseInterested = await axios.get("api/interestedproduct");

        const { data: interested } = responseInterested.data;

        setInterested(interested);

        const responseSold = await axios.get("api/soldproduct");

        if (responseSold.data.status) {
          setSold(responseSold.data.data);
        }
      } catch (error) {
        console.log(error);
      }
    };

    getProductUser();
  }, []);

  const navigateProduct = (id) => {
    navigate(`/productpage/${id}`);
  };

  return (
    <Container className="mt-4 pb-4">
      <div className="mb-4 title-list">
        <h4>Daftar Jual Saya</h4>
      </div>

      <Card className="seller">
        <Card.Body>
          {!user.image && (
            <div className="profile">
              <FontAwesomeIcon icon="fa-user-alt" size="lg" />
            </div>
          )}
          <div className="d-flex flex-column justify-content-center">
            <h5 style={{ margin: 0 }}>{user.full_name}</h5>
            <p style={{ margin: 0 }}>
              {user.address ? user.address : "Tidak diketahui"}
            </p>
          </div>
          <div className="seller__cta">
            <Link to="/profile" className="seller-btn">
              Edit
            </Link>
          </div>
        </Card.Body>
      </Card>
      <Row className="mt-4">
        <Col>
          <div className="card category">
            <div className="card-body">
              <h5 className="px-3 pt-1">Kategori</h5>
              <ListGroup className="py-2 text-decoration" variant="flush">
                {menus.map((menu) => (
                  <ListGroup.Item
                    action
                    onClick={() => setMenuActive(menu.value)}
                    className="py-3"
                    key={menu.value}
                  >
                    <i
                      type="button"
                      className={`"link" ${
                        menuActive === menu.value ? "active" : ""
                      }`}
                    >
                      {menu.icon} {menu.text}
                    </i>
                  </ListGroup.Item>
                ))}
              </ListGroup>
            </div>
          </div>
        </Col>
        <Col xs lg="9">
          {menuActive === "all" && (
            <Row lg="3" style={{ rowGap: "1em" }}>
              <Col>
                <Link
                  className="card content add"
                  type="button"
                  to="/productinfo"
                  state={"/daftarjual"}
                >
                  <FaPlus /> <p className="pt-2">Tambah Produk</p>
                </Link>
              </Col>
              {products.length > 0 &&
                products.map((product) => (
                  <Col key={product.id}>
                    <Card
                      className="content"
                      onClick={() => navigateProduct(product.id)}
                    >
                      {product.image.length > 0 ? (
                        <img
                          src={baseURL + product.image[0]?.path}
                          className="card-img-top"
                          alt={"Product " + product.name}
                          style={{ height: "100px", objectFit: "contain" }}
                        />
                      ) : (
                        <div
                          className="card-img-top"
                          style={{
                            height: "100px",
                            objectFit: "contain",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                          }}
                        >
                          <FontAwesomeIcon icon="fa-image" size="5x" />
                        </div>
                      )}
                      <Card.Body>
                        <p className="card-title">{product.name}</p>
                        <p className="card-text">{product.category.name}</p>
                        <p className="card-price">
                          {new Intl.NumberFormat("id-ID", {
                            style: "currency",
                            currency: "idr",
                          }).format(product.price)}
                        </p>
                      </Card.Body>
                    </Card>
                  </Col>
                ))}
            </Row>
          )}
          {menuActive === "whislist" &&
            (interested.length > 0 ? (
              <Row lg="3" style={{ rowGap: "1em" }}>
                {interested.map((whislist) => (
                  <Col key={whislist.product.id}>
                    <Card
                      className="content"
                      onClick={() => navigateProduct(whislist.product.id)}
                    >
                      {whislist.product.image.length > 0 ? (
                        <img
                          src={baseURL + whislist.product.image[0]?.path}
                          className="card-img-top"
                          alt={"Product " + whislist.product.name}
                          style={{ height: "100px", objectFit: "contain" }}
                        />
                      ) : (
                        <div
                          className="card-img-top"
                          style={{
                            height: "100px",
                            objectFit: "contain",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                          }}
                        >
                          <FontAwesomeIcon icon="fa-image" size="5x" />
                        </div>
                      )}
                      <Card.Body>
                        <p className="card-title">{whislist.product.name}</p>
                        <p className="card-text">
                          {whislist.product.category.name}
                        </p>
                        <p className="card-price">
                          {new Intl.NumberFormat("id-ID", {
                            style: "currency",
                            currency: "idr",
                          }).format(whislist.product.price)}
                        </p>
                      </Card.Body>
                    </Card>
                  </Col>
                ))}
              </Row>
            ) : (
              <div className="d-flex justify-content-center">
                <div style={{ maxWidth: "290px" }}>
                  <img src={undrawSelection} alt="Tidak ada produk" />
                  <p className="text-center mt-2">
                    Belum ada produkmu yang diminati nih, sabar ya rejeki nggak
                    kemana kok
                  </p>
                </div>
              </div>
            ))}

          {menuActive === "sold" &&
            (sold.length > 0 ? (
              <Row lg="3" style={{ rowGap: "1em" }}>
                {sold.map((soldProduct) => (
                  <Col key={soldProduct.product.id}>
                    <Card
                      className="content"
                      onClick={() => navigateProduct(soldProduct.product.id)}
                    >
                      {soldProduct.product.image.length > 0 ? (
                        <img
                          src={baseURL + soldProduct.product.image[0]?.path}
                          className="card-img-top"
                          alt={"Product " + soldProduct.product.name}
                          style={{ height: "100px", objectFit: "contain" }}
                        />
                      ) : (
                        <div
                          className="card-img-top"
                          style={{
                            height: "100px",
                            objectFit: "contain",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                          }}
                        >
                          <FontAwesomeIcon icon="fa-image" size="5x" />
                        </div>
                      )}
                      <Card.Body>
                        <p className="card-title">{soldProduct.product.name}</p>
                        <p className="card-text">
                          {soldProduct.product.category.name}
                        </p>
                        <p className="card-price">
                          {new Intl.NumberFormat("id-ID", {
                            style: "currency",
                            currency: "idr",
                          }).format(soldProduct.product.price)}
                        </p>
                      </Card.Body>
                    </Card>
                  </Col>
                ))}
              </Row>
            ) : (
              <div className="d-flex justify-content-center">
                <div style={{ maxWidth: "290px" }}>
                  <img src={undrawSelection} alt="Tidak ada produk" />
                  <p className="text-center mt-2">
                    Belum ada produkmu yang terjual nih, sabar ya rejeki nggak
                    kemana kok
                  </p>
                </div>
              </div>
            ))}
        </Col>
      </Row>
    </Container>
  );
}

export default ListProduct;

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import Cookies from "js-cookie";
import jwtDecode from "jwt-decode";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { login, logout } from "../app/features/userSlice";

export default function Header() {
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const loggedIn = async () => {
    const getToken = Cookies.get("token");
    if (getToken) {
      const decode = jwtDecode(getToken);
      axios.defaults.headers.common["Authorization"] = "Bearer " + getToken;
      try {
        const response = await axios.get(`/api/profile/${decode.id}`);
        const data = response.data;

        dispatch(login({ token: getToken, data: data.data }));
      } catch (error) {
        console.log(error);
      }
    }
  };

  useEffect(() => {
    loggedIn();
  }, []);

  const logoutAction = function () {
    Cookies.remove("token");
    navigate("/");
    dispatch(logout());
  };

  return (
    <nav className="navbar navbar-expand-lg sticky-top">
      <div className="container">
        <Link className="navbar-brand" to="/">
          &nbsp;
        </Link>
        <form className="search-bar">
          {/* <input
            className="form-control cari-produk"
            type="search"
            placeholder="Cari di sini ..."
            aria-label="Search"
          /> */}
          <input
            className="form-control cari-produk"
            type="text"
            placeholder="Cari disini..."
            aria-label="Search"
          />
          {/* <i class="fas fa-search" aria-hidden="true"></i> */}
          <FontAwesomeIcon icon="fa-search" />
        </form>
        <div className="navbar-nav navbar-top">
          <div className="collapse navbar-collapse" id="navbarText">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              {Object.keys(user.user).length === 0 ? (
                <>
                  <li className="nav-item-login">
                    <FontAwesomeIcon icon="fa-sign-in" />
                    <Link to="/login">Masuk</Link>
                  </li>
                </>
              ) : (
                <>
                  <li>
                    <NavLink
                      style={({ isActive }) => ({
                        color: isActive ? "#7126B5" : "black",
                      })}
                      className="nav-link"
                      aria-current="page"
                      to="/daftarjual"
                    >
                      <FontAwesomeIcon icon="fa-list" size="lg" />
                    </NavLink>
                  </li>
                  <li className="dropdown">
                    <button
                      id="notification"
                      type="button"
                      className="btn btn-link"
                      data-toggle="dropdown"
                      data-display="static"
                      aria-expanded="false"
                    >
                      <FontAwesomeIcon icon="fa-bell" size="lg" />
                    </button>
                    <div
                      className="dropdown-menu dropdown-menu-right"
                      aria-labelledby="user"
                    >
                      <NavLink className="dropdown-item" to="/profile">
                        Profile
                      </NavLink>
                      <div className="dropdown-divider"></div>
                      <button className="dropdown-item">Log Out</button>
                    </div>
                  </li>
                  <li className="dropdown">
                    <button
                      id="user"
                      type="button"
                      className="btn btn-link"
                      data-toggle="dropdown"
                      data-display="static"
                      aria-expanded="false"
                    >
                      <FontAwesomeIcon icon="fa-user" size="lg" />
                    </button>
                    <div
                      className="dropdown-menu dropdown-menu-right"
                      aria-labelledby="user"
                    >
                      <NavLink className="dropdown-item" to="/profile">
                        Profile
                      </NavLink>
                      <div className="dropdown-divider"></div>
                      <button
                        className="dropdown-item btn"
                        onClick={logoutAction}
                      >
                        Log Out
                      </button>
                    </div>
                  </li>
                </>
              )}
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
}

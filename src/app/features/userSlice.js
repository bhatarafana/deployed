import { createSlice } from "@reduxjs/toolkit";


const userSlice = createSlice({
    name: "user",
    initialState: {
        token: null,
        loggedIn: false,
        user: {},
    },
    reducers: {
        login: (state, { payload }) => {
            state.token = payload.token
            state.loggedIn = true
            state.user = payload.data
        },
        logout: (state, action) => {
            state.token = null
            state.loggedIn = false
            state.user = {}
        }

    }
})

export const { login, logout } = userSlice.actions

export default userSlice.reducer